# OS View 

Screenshot: 

![title](app/src/main/res/drawable/os_view_screenshot.png)



Method Usage : 

 1. Fragment 
 3. TabLayout
 4. AppBarLayout 
 5. Toolbar 
 6. ViewPager 
 7. ImageButton
 8. Custom Adapter
  8. ActionBar Menu



Library : 

1. ```xml
    implementation 'com.google.android.material:material:1.1.0'
    ```
2. ```xml
	implementation 'com.intuit.sdp:sdp-android:1.0.6'
	```

Source : 
1. windows xp - [wikipedia](https://bn.wikipedia.org/wiki/%E0%A6%89%E0%A6%87%E0%A6%A8%E0%A7%8D%E0%A6%A1%E0%A7%8B%E0%A6%9C_%E0%A6%8F%E0%A6%95%E0%A7%8D%E0%A6%B8%E0%A6%AA%E0%A6%BF)
2. windows vista - [wikipedia](https://bn.wikipedia.org/wiki/%E0%A6%89%E0%A6%87%E0%A6%A8%E0%A7%8D%E0%A6%A1%E0%A7%8B%E0%A6%9C_%E0%A6%AD%E0%A6%BF%E0%A6%B8%E0%A7%8D%E0%A6%A4%E0%A6%BE)
3. windows 7 - [wikipedia](https://bn.wikipedia.org/wiki/%E0%A6%89%E0%A6%87%E0%A6%A8%E0%A7%8D%E0%A6%A1%E0%A7%8B%E0%A6%9C_%E0%A7%AD)
4. windows 10 - [wikipedia](https://bn.wikipedia.org/wiki/%E0%A6%89%E0%A6%87%E0%A6%A8%E0%A7%8D%E0%A6%A1%E0%A7%8B%E0%A6%9C_%E0%A7%A7%E0%A7%A6)
5. android versions - [wikipedia](https://en.wikipedia.org/wiki/Android_version_history)

