package com.example.osoverview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // android button
        ImageButton a_button = findViewById(R.id.android_button);
        a_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, android_activity.class);
                startActivity(i);
            }
        });

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    /*
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()){
                case R.id.menu1:
                    Toast.makeText(this, "Clicked Menu 1", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.menu2:
                    Toast.makeText(this, "Clicked Menu 2", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
     */

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {

            final Toast toast = Toast.makeText(getApplicationContext(), "About Me", Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            },500); // delay time

            // adding intent
            Intent i = new Intent(MainActivity.this, about_me.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // about me menu
    public void about_menu(MenuItem item){

    }


    // windows button
    public void windows_button_click(View v) {
        //Toast.makeText(getApplicationContext(), "windows", Toast.LENGTH_LONG).show();

        // reducing delay time
        final Toast toast = Toast.makeText(getApplicationContext(), "Windows OS", Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time

        // adding intent
        Intent i = new Intent(MainActivity.this, windows_activity.class);
        startActivity(i);

    }

}