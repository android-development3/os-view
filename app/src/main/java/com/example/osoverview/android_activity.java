package com.example.osoverview;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;

public class android_activity extends AppCompatActivity {

    private TabLayout my_tl;
    private ViewPager my_vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.android_fragment);

        my_vp = findViewById(R.id.android_view_pager); // windows view pager
        my_tl = findViewById(R.id.android_tab); // windows tabs

        setUpMyViewPager(my_vp);
        my_tl.setupWithViewPager(my_vp);

    }

    void setUpMyViewPager(ViewPager vp){

        windows_activity.ViewPagerAdapter vpa = new windows_activity.ViewPagerAdapter(getSupportFragmentManager());
        vpa.addMyFragment(new a_cupcake(), "Cupcake");
        vpa.addMyFragment(new a_donut(), "Donut");
        vpa.addMyFragment(new a_eclair(), "Eclair");
        vpa.addMyFragment(new a_froyo(),"Froyo");
        vpa.addMyFragment(new a_gingerbread(),"Gingerbread");
        vpa.addMyFragment(new a_ice_cream(),"Ice Cream Sandwich");

        vp.setAdapter(vpa);

    }


    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> my_list = new ArrayList<Fragment>();
        private final List<String> my_titles = new ArrayList<String>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return my_list.get(position);
        }

        @Override
        public int getCount() {
            return my_list.size();
        }

        void addMyFragment(Fragment f, String title){
            my_list.add(f);
            my_titles.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return my_titles.get(position);
        }

    }

}